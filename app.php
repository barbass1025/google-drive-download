<?php

set_time_limit(1800);
require_once (__DIR__.'/vendor/autoload.php');
require_once (__DIR__.'/config.php');

$client = new Google_Client();
$client->setAuthConfig($config['client_secret_file']);
$client->setAccessType("offline");        // offline access
$client->setIncludeGrantedScopes(true);   // incremental auth
$client->addScope($config['google_api']);

if (file_exists($config['token_file'])) {
	$access_token = unserialize(file_get_contents($config['token_file']));
} else {
	$access_token = false;
}

if (!file_exists($config['to_path'])) {
	$result = mkdir($config['to_path'], 0744, true);
	if (!$result) {
		throw new \Exception('Error create ' . $config['to_path']);
	}
}

if ($access_token) {
	$client->setAccessToken($access_token);
	$drive = new Google_Service_Drive($client);
	echo microtime().'<br>';
	request_files($client, $drive, $config['to_path'], $config['google_path_id']);
	echo microtime().'<br>';
} else {
	if (isset($_GET['code'])) {
		$access_token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
		file_put_contents('token.txt', serialize($access_token));
		header('Location: ' . filter_var($config['redirect_url'], FILTER_SANITIZE_URL));
	} else {
		$client->setRedirectUri($config['redirect_url']);
		$auth_url = $client->createAuthUrl();
		header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
	}
}

function request_files($client, $drive, $folder, $parent) {
	echo('<ul>');
	$pageToken = null;
	do {
		$files = $drive->files->listFiles([
			'q' => "'".$parent."' in parents and trashed = false",
			'corpora' => 'user',
			'pageToken' => $pageToken,
			'spaces' => 'drive',
			'fields' => 'nextPageToken, files(id, name, size, modifiedTime, mimeType, capabilities, md5Checksum)'
		]);
		foreach ($files->files as $file) {
			if ($file->capabilities->canDownload != '1') {
				continue;
			}

			printf("<li><b>%s</b> (%s), <i>%s</i> %s", $file->name, $file->id, $file->mimeType, explode('.', $file->modifiedTime)[0]);

			if (stristr(PHP_OS, 'WIN')) {
				$file_name = trim(mb_convert_encoding($file->name, 'CP1251', 'UTF-8'));
			} elseif(stristr(PHP_OS, 'LINUX')) {
				$file_name = trim($file->name);
			}

			$file_name = preg_replace('/(\\|\/|\:|\*|\?|\"|\<|\>|\||\+)/iu', '', $file_name);
			$file_path = $folder.'/'.$file_name;

			if ($file->mimeType === 'application/vnd.google-apps.folder') {
				$fn = $file_path;
				if (!file_exists($fn)) {
					$result = mkdir($fn, 0744);
					if (!$result) {
						throw new \Exception('Error create ' . $fn);
					}
				}
				request_files($client, $drive, $file_path, $file->id);

				continue;
			}

			// Если файл, сверяем хэш
			if ($file->mimeType !== 'application/vnd.google-apps.folder') {
				if (file_exists($file_path) && !empty($file->md5Checksum)) {
					$md5_fn = md5_file($file_path);
					$md5_new_file = $file->md5Checksum;

					if ($md5_fn === $md5_new_file) {
						continue;
					}
				}
			}

			if ($file->size < 1048576) {
				switch($file->mimeType) {
					case 'application/vnd.google-apps.document':
						$fn = $file_path.'.docx';
						$response = $drive->files->export($file->id, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', array('alt' => 'media'));
						break;
					case 'application/vnd.google-apps.spreadsheet':
						$fn = $file_path.'.xlsx';
						$response = $drive->files->export($file->id, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', array('alt' => 'media'));
						break;
					case 'application/vnd.google-apps.presentation':
						$fn = $file_path.'.pptx';
						$response = $drive->files->export($file->id, 'application/vnd.openxmlformats-officedocument.presentationml.presentation', array('alt' => 'media'));
						break;
					case 'application/vnd.google-apps.form':
						continue;
						break;
					default:
						$fn = $file_path;
						$response = $drive->files->get($file->id, array('alt' => 'media'));
						break;
				}

				try {
					$content = (!empty($response)) ? $response->getBody()->getContents() : null;
				} catch(\Exception $e) {
					continue;
				}

				if (!$content) {
					continue;
				}

				file_put_contents($fn, $content);
			} else {
				$fileSize = intval($file->size);
				$http = $client->authorize();
				$fp = fopen($file_path, 'w');
				$chunkSizeBytes = 1 * 1024 * 1024; //1Mb
				$chunkStart = 0;
				while ($chunkStart < $fileSize) {
					$chunkEnd = $chunkStart + $chunkSizeBytes;
					$response = $http->request(
						'GET',
						sprintf('/drive/v3/files/%s', $file->id),
						[
							'query' => ['alt' => 'media'],
							'headers' => [
								'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
							]
						]
					);
					$chunkStart = $chunkEnd + 1;
					fwrite($fp, $response->getBody()->getContents());
				}
				fclose($fp);
			}
			echo('</li>');
			usleep(100000);
		}
		$pageToken = $files->nextPageToken;
	} while ($pageToken != null);
	echo('</ul>');
}
