<?php

$config = [
	'google_api' => 'https://www.googleapis.com/auth/drive',
	'google_path_id' => '',

	'to_path' => __DIR__.'/files/',
	'client_secret_file' => __DIR__.'/client_secret.json',
	'token_file' => __DIR__.'/token.txt',
	'redirect_url' => 'http://localhost/script/google_drive/drive_oauth.php',
];
