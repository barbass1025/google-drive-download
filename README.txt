﻿1) https://console.developers.google.com/
Библиотека -> Google Apps API -> Drive API
Выйдет запрос на создание проекта.
Включить API
Создать учетные данные -> идентификатор клиента Oauth
В redirect uri нужно указать адрес, куда будет перенаправлять из гугл диска после авторизации.
Скачать json-файл и положить его в папку со скриптом ('client_secret.json')
B $download_folder надо указать ID папки с google drive, которую скачивать.
Создать папку, в которую скачивать файлы ($storage_folder).

2) в php.ini прописать 
curl.cainfo =D:\work\xampp708\htdocs\script\google_drive\cacert.pem

3) Пример свойств файла/каталога
Google_Service_Drive_DriveFile Object ( [collection_key:protected] =>
spaces [appProperties] => [capabilitiesType:protected] =>
Google_Service_Drive_DriveFileCapabilities
[capabilitiesDataType:protected] => [contentHintsType:protected] =>
Google_Service_Drive_DriveFileContentHints
[contentHintsDataType:protected] => [createdTime] => [description] =>
[explicitlyTrashed] => [fileExtension] => [folderColorRgb] =>
[fullFileExtension] => [hasAugmentedPermissions] => [hasThumbnail] =>
[headRevisionId] => [iconLink] => [id] => 0B6rkBA2XPEAPcVVldjY0N79sWkU
[imageMediaMetadataType:protected] =>
Google_Service_Drive_DriveFileImageMediaMetadata
[imageMediaMetadataDataType:protected] => [isAppAuthorized] => [kind]
=> [lastModifyingUserType:protected] => Google_Service_Drive_User
[lastModifyingUserDataType:protected] => [md5Checksum] => [mimeType] =>
application/vnd.google-apps.folder [modifiedByMe] => [modifiedByMeTime]
=> [modifiedTime] => 2017-03-31T09:44:31.307Z [name] => ЕРЦ - Обмен
[originalFilename] => [ownedByMe] => [ownersType:protected] =>
Google_Service_Drive_User [ownersDataType:protected] => array [parents]
=> [permissionsType:protected] => Google_Service_Drive_Permission
[permissionsDataType:protected] => array [properties] =>
[quotaBytesUsed] => [shared] => [sharedWithMeTime] =>
[sharingUserType:protected] => Google_Service_Drive_User
[sharingUserDataType:protected] => [size] => [spaces] => [starred] =>
[teamDriveId] => [thumbnailLink] => [thumbnailVersion] => [trashed] =>
[trashedTime] => [trashingUserType:protected] =>
Google_Service_Drive_User [trashingUserDataType:protected] => [version]
=> [videoMediaMetadataType:protected] =>
Google_Service_Drive_DriveFileVideoMediaMetadata
[videoMediaMetadataDataType:protected] => [viewedByMe] =>
[viewedByMeTime] => [viewersCanCopyContent] => [webContentLink] =>
[webViewLink] => [writersCanShare] =>
[internal_gapi_mappings:protected] => Array ( ) [modelData:protected]
=> Array ( [capabilities] => Array ( [canAddChildren] => 1
[canChangeViewersCanCopyContent] => 1 [canComment] => 1 [canCopy] =>
[canDelete] => 1 [canDownload] => 1 [canEdit] => 1 [canListChildren] =>
1 [canMoveItemIntoTeamDrive] => 1 [canReadRevisions] =>
[canRemoveChildren] => 1 [canRename] => 1 [canShare] => 1 [canTrash] =>
1 [canUntrash] => 1 ) ) [processed:protected] => Array ( ) )