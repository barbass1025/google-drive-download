<?php

class Drive {
	protected $config = [];
	protected $google_client = null;

	public function __construct($config) {
		$this->config = $config;
	}

	public function getGoogleClient() {
		$this->google_client = new Google_Client();
		$this->google_client->setAuthConfig($config['client_secret_file']);
		$this->google_client->setAccessType("offline");        // offline access
		$this->google_client->setIncludeGrantedScopes(true);   // incremental auth
		$this->google_client->addScope($config['google_api']);
	}

	public function checkToken() {
		if (file_exists($this->config['token_file'])) {
			$access_token = unserialize(file_get_contents($config['token_file']));
		} else {
			$access_token = false;
		}

		return false;
	}

	public function getFiles($client, $drive, $folder, $parent) {

	}

}
